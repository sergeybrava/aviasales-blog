<?php get_header(); ?>
<div class="breadcrumbs">
  <ul class="breadcrumbs__list">
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/">Главная</a>
    </li>
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/">Блог</a>
    </li>
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/">Архив рассылки</a>
    </li>
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/">Самый красивый вид в жизни путешественника</a>
    </li>
  </ul>
</div>
<section class="main">
  <article class="article">
    <p class="article__teaser">
      На носу горнолыжный сезон, и Грузия как всегда займёт топовые позиции в рейтинге у россиян, конечно, после наших Сочи и Минеральных Вод. Популярность Грузии легко объяснить: виза не нужна, лететь недалеко, большинство местных жителей говорит по-русски, вкусная еда и недорогой сервис. Но чтобы максимально сэкономить на поездке в снежные горы, предлагаем уже сейчас заняться планированием. Aviasales подскажет, когда покупать билеты и на какие даты дешевле, с какой авиакомпанией выгоднее лететь, сколько стоит ски-пасс и проживание в горнолыжных курортах.
    </p>
    <h2>Когда дешевле лететь в горнолыжную Грузию</h2>
    <p class="subheader">7 октября, стадион «Ле динамьё», Париж, Франиця</p>
    <p>Самый верный способ узнать, когда дешевле полететь в Грузию, — воспользоваться календарём низких цен. Например, самые дешёвые билеты на данный момент доступны на январь, февраль и начало марта. То есть билеты за 6 тысяч рублей из Москвы в обе стороны можно поймать за 3 месяца до покупки.</p>
    <p>Давайте отдельно сравним стоимость прямых и пересадочных рейсов, а также минимальные цены у авиакомпаний, которые летают из Москвы в Грузию.</p>
    <h2>Сколько стоит провоз лыж и сноуборда: сравнение авиакомпаний</h2>
    <p>
      <img src="http://blog.aviasales.ru/wp-content/uploads/2016/10/novyj-god-kuda-nedorogo-poletet.png" alt="">
      Самый верный способ узнать, когда дешевле полететь в Грузию, — воспользоваться календарём низких цен. Например, самые дешёвые билеты на данный момент доступны на январь, февраль и начало марта. То есть билеты за 6 тысяч рублей из Москвы в обе стороны можно поймать за 3 месяца до покупки.
    </p>
    <p>Давайте отдельно сравним стоимость прямых и пересадочных рейсов, а также минимальные цены у авиакомпаний, которые летают из Москвы в Грузию.</p>
    <h2>Когда дешевле лететь в горнолыжную Грузию</h2>
    <p class="subheader">7 октября, стадион «Ле динамьё», Париж, Франиця</p>
    <p>Самый верный способ узнать, когда дешевле полететь в Грузию, — воспользоваться календарём низких цен. Например, самые дешёвые билеты на данный момент доступны на январь, февраль и начало марта. То есть билеты за 6 тысяч рублей из Москвы в обе стороны можно поймать за 3 месяца до покупки.</p>
    <p>Давайте отдельно сравним стоимость прямых и пересадочных рейсов, а также минимальные цены у авиакомпаний, которые летают из Москвы в Грузию.</p>
  </article>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
