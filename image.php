<?php 
if (!$post->post_parent || $post->post_parent = 0) {
	global $wp_query;
	$wp_query->set_404();
	get_header();?>
	<div class='categories-top-wrapper is-phablet-hidden'>
		<?php
		aviasales_get_categories();
		?>
	</div>
	<?php
	get_sidebar();
	get_footer();
} else {
	wp_redirect(get_permalink($post->post_parent), 301);
}

?>