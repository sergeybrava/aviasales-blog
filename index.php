<?php get_header(); ?>
<?php if (is_single()): ?>
<?php the_content(); ?>
<?php else : ?> 
<?php get_template_part('loop'); ?>
<div class="paging">
    <?php aviasales_pagination(); ?>
</div>
<?php endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
