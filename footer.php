<?php
/**
 * The template for displaying the sidebar & footer.
 */
?>
			</div></div><!-- #center_content--><!-- #container-->

			<div id="side_right">

				<?php dynamic_sidebar( 'sidebar-1' ); ?>

			</div><!-- #side_right -->

		</div><!-- #content_wrapper -->

	</div><!-- #content-->

</div><!-- #wrapper -->

<?php dynamic_sidebar('after-3-teasers') ?><!-- #aviasales form widget -->
<?php dynamic_sidebar('after-6-teasers') ?><!-- #hotellook form widget -->

<div class="mobile-applications">
    <div class="mobile-applications__wrapper">
        <div class="mobile-applications__img"></div>
        <div class="mobile-applications__content">
            <div class="mobile-applications__title">Скачай мобильное приложение Aviasales.ru</div>
            <div class="mobile-applications__stars">Более 103 000 оценок</div>
            <ul class="mobile-applications__list">
                <li class="mobile-applications__item"><a class="app-link --ios" data-goal="ios_app_link" href="/ios?c=bottom_banner" target="_blank">iPhone или iPad</a></li>
                <li class="mobile-applications__item"><a class="app-link --android" data-goal="android_app_link" href="/android?c=bottom_banner" target="_blank">Android</a></li>
                <li class="mobile-applications__item"><a class="app-link --winphone" data-goal="wphone_app_link" href="//avs.io/landing" target="_blank">Windows Phone</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="footer" data-goal-category="footer">
    <div class="footer__inner">
        <div class="footer__links footer__side-spaces">
            <nav>
                <div class="footer__title">Страны</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/countries/rossiya" title="Авиабилеты в Россию">Россия</a></li>
                    <li><a href="https://www.aviasales.ru/countries/tailand" title="Авиабилеты в Таиланд">Таиланд</a></li>
                    <li><a href="https://www.aviasales.ru/countries/chernogoriya" title="Авиабилеты в Черногорию">Черногория</a></li>
                    <li><a href="https://www.aviasales.ru/countries/kipr" title="Авиабилеты в Кипр">Кипр</a></li>
                    <li><a href="https://www.aviasales.ru/countries/bolgariya" title="Авиабилеты в Болгарию">Болгария</a></li>
                    <li><a href="https://www.aviasales.ru/countries/gruziya" title="Авиабилеты в Грузию">Грузия</a></li>
                    <li class="footer__all"><a href="https://www.aviasales.ru/countries">Все&nbsp;страны</a></li>
                </ul>
            </nav>
            <nav>
                <div class="footer__title">Города</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/cities/moskva-mow" title="Авиабилеты в Москву">Москва</a></li>
                    <li><a href="https://www.aviasales.ru/cities/sankt-peterburg-led" title="Авиабилеты в Санкт-Петербург">Санкт-Петербург</a></li>
                    <li><a href="https://www.aviasales.ru/cities/simferopol-sip" title="Авиабилеты в Симферополь">Симферополь</a></li>
                    <li><a href="https://www.aviasales.ru/cities/adler-aer" title="Авиабилеты в Адлер">Адлер</a></li>
                    <li><a href="https://www.aviasales.ru/cities/ekaterinburg-svx" title="Авиабилеты в Екатеринбург">Екатеринбург</a></li>
                    <li><a href="https://www.aviasales.ru/cities/london-lon" title="Авиабилеты в Лондон">Лондон</a></li>
                    <li class="footer__all"><a href="https://www.aviasales.ru/cities">Все&nbsp;города</a></li>
                </ul>
            </nav>
            <nav>
                <div class="footer__title">Авиакомпании</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/airlines/airberlin" title="Спецпредложения авиакомпании Air Berlin">Air&nbsp;Berlin</a></li>
                    <li><a href="https://www.aviasales.ru/airlines/air-france" title="Спецпредложения авиакомпании Air France">Air&nbsp;France</a></li>
                    <li><a href="https://www.aviasales.ru/airlines/alitalia" title="Спецпредложения авиакомпании Alitalia">Alitalia</a></li>
                    <li><a href="https://www.aviasales.ru/airlines/airbaltic" title="Спецпредложения авиакомпании Air Baltic">Air&nbsp;Baltic</a></li>
                    <li><a href="https://www.aviasales.ru/airlines/emirates" title="Спецпредложения авиакомпании Emirates">Emirates</a></li>
                    <li><a href="https://www.aviasales.ru/airlines/klm" title="Спецпредложения авиакомпании KLM">KLM</a></li>
                    <li class="footer__all"><a href="https://www.aviasales.ru/airlines">Все&nbsp;авиакомпании</a></li>
                </ul>
            </nav>
            <nav>
                <div class="footer__title">Аэропорты</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/airports/sheremetevo" title="Аэропорт Шереметьево Москва, Россия">Шереметьево</a></li>
                    <li><a href="https://www.aviasales.ru/airports/kurumoch" title="Аэропорт Курумоч Самара, Россия">Курумоч</a></li>
                    <li><a href="https://www.aviasales.ru/airports/domodedovo" title="Аэропорт Домодедово Москва, Россия">Домодедово</a></li>
                    <li><a href="https://www.aviasales.ru/airports/tolmachevo" title="Аэропорт Толмачево Новосибирск, Россия">Толмачево</a></li>
                    <li><a href="https://www.aviasales.ru/airports/vladivostok" title="Аэропорт Владивосток Владивосток, Россия">Владивосток</a></li>
                    <li><a href="https://www.aviasales.ru/airports/aeroport-gamburga" title="Аэропорт Гамбурга Гамбург, Германия">Гамбург</a></li>
                    <li class="footer__all"><a href="https://www.aviasales.ru/airports">Все&nbsp;аэропорты</a></li>
                </ul>
            </nav>
            <nav>
                <div class="footer__title">Направления</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/routes/mow/sip" title="Авиабилеты из Москвы в Симферополь (Крым)">MOW – SIP</a></li>
                    <li><a href="https://www.aviasales.ru/routes/mow/aer" title="Авиабилеты из Москвы в Сочи">MOW – AER</a></li>
                    <li><a href="https://www.aviasales.ru/routes/mow/tiv" title="Авиабилеты из Москвы в Тиват">MOW – TIV</a></li>
                    <li><a href="https://www.aviasales.ru/routes/mow/mrv" title="Авиабилеты из Москвы в Минеральные Воды">MOW – MRV</a></li>
                    <li><a href="https://www.aviasales.ru/routes/led/mow" title="Авиабилеты из Санкт-Петербурга в Москву">LED – MOW</a></li>
                    <li><a href="https://www.aviasales.ru/routes/mow/bkk" title="Авиабилеты из Москвы в Бангкок">MOW – BKK</a></li>
                </ul>
            </nav>
            <nav>
                <div class="footer__title">Сервисы</div>
                <ul class="footer__list">
                    <li><a href="https://www.aviasales.ru/calendar" title="Календарь низких цен">Календарь низких цен</a></li>
                    <li><a href="https://map.aviasales.ru/" title="Карта низких цен">Карта низких цен</a></li>
                    <li><a href="https://www.aviasales.ru/offers" title="Спецпредложения">Спецпредложения</a></li>
                    <li><a href="http://engine.aviasales.ru/latest_prices" title="Таблица цен">Таблица цен</a></li>
                    <li><a href="https://www.aviasales.ru/blog" rel="nofollow" target="_blank" title="Блог">Блог</a></li>
                    <li><a href="https://www.aviasales.ru/mag/how-to" title="Помощь">Помощь</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="footer__inner">
            <div class="footer__side-spaces">
                <div class="footer__side">
                    <ul class="footer__links-list">
                        <li><a href="https://www.aviasales.ru/about">О компании</a></li>
                        <li><a href="https://www.aviasales.ru/affiliateprogram">Партнёрская программа</a></li>
                        <li><a href="https://www.aviasales.ru/advertising">Реклама</a></li>
                        <li><a href="https://www.aviasales.ru/vacancies">Вакансии</a></li>
                        <li><a href="https://www.aviasales.ru/mag/how-to">Помощь</a></li>
                        <li><a href="https://www.aviasales.ru/terms-of-use">Правила</a></li>
                        <li><a href="https://www.travelpayouts.com/promo/whitelabel/?utm_source=aviasales" rel="nofollow" target="_blank">White Label авиабилеты</a></li>
                    </ul>
                    <ul class="footer__links-list --social">
                        <li class="vk"><a data-goal="vk_group_link" href="https://vk.com/aviasalesru" rel="nofollow" target="_blank" title="Aviasales Вконтакте">Вконтакте</a></li>
                        <li class="fb"><a data-goal="fb_group_link" href="https://www.facebook.com/aviasales.ru" rel="nofollow" target="_blank" title="Aviasales в Facebook">Фейсбук</a></li>
                        <li class="ig"><a data-goal="insta_group_link" href="https://instagram.com/aviasales" rel="nofollow" target="_blank" title="Aviasales в Instagram">Инстаграм</a></li>
                        <li class="tw"><a data-goal="tw_group_link" href="https://twitter.com/aviasales" rel="nofollow" target="_blank" title="Aviasales в Twitter">Твиттер</a></li>
                        <li class="vb"><a data-goal="vb_group_link" href="http://chats.viber.com/aviasales" rel="nofollow" target="_blank" title="Aviasales Вайбер">Вайбер</a></li>
                    </ul>
                    <ul class="footer__links-list">
                        <li><a data-goal="hotellook_link" data-markerable="" href="https://hotellook.ru/?marker=119511" rel="nofollow" target="_blank">Поиск и бронирование отелей</a></li>
                    </ul>
                </div>
                <div class="footer__side --right">
                    <div class="apps-list"><a class="apps-list__item --as" data-goal="ios_app_link" href="https://www.aviasales.ru/ios?c=footer_menu" target="_blank">Скачайте в <div class="apps-list__title" data-goal="ios_app_link">App Store</div></a><a class="apps-list__item --gp" data-goal="android_app_link"
                            href="https://www.aviasales.ru/android?c=footer_menu" target="_blank">Скачайте в <div class="apps-list__title" data-goal="android_app_link">Google Play</div></a>
                        <a class="apps-list__item --wp" data-goal="wphone_app_link" href="//avs.io/landing" target="_blank">
                            <div class="apps-list__title" data-goal="wphone_app_link">Windows Phone</div>
                        </a>
                    </div>
                    <div class="footer__copy">© 2007–2017, Aviasales — дешевые авиабилеты</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
				<div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

<?php dynamic_sidebar('footer') ?>

<?php wp_footer(); ?>

<!-- Yandex.Metrika global start - do not remove!!! -->
    <script type="text/javascript">
    (function(d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter3791698 = w.yaCounter3791698 = new Ya.Metrika({
                    id: 3791698,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {}
        });
    }(document, window, 'yandex_metrika_callbacks'));
    </script>
    <script async="" defer="defer" src="https://mc.yandex.ru/metrika/watch.js"></script>
<!-- Yandex.Metrika global finish -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-1481416-4', 'auto', {'legacyCookieDomain': 'aviasales.ru', name: 'aviasales_all'});
    ga('aviasales_all.send', 'pageview');
    ga('require', 'ecommerce')
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1481416-17']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21025009 = new Ya.Metrika({id:21025009, webvisor:true, clickmap:true, trackLinks:true, triggerEvent:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/21025009" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<script>
    (function (w) {
    w.mamka = w.mamka || function(){(mamka.queue=mamka.queue||[]).push(arguments)};
    }(window));
    mamka('create', {'project_name': 'aviasales_blog_wp', 'brand_names': ['aviasales']})
</script>
<script async type="text/javascript" src="https://mamka.aviasales.ru/mamka.js?v=1"></script>

<!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5KVV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5KVV');</script>
<!-- End Google Tag Manager -->

<script>
var _rollbarConfig = {
    accessToken: "80366a21ad58428eb7a5538f450af58e",
    captureUncaught: true,
    captureUnhandledRejections: true,
    payload: {
        environment: "production"
    }
};
// Rollbar Snippet
!function(r){function e(n){if(o[n])return o[n].exports;var t=o[n]={exports:{},id:n,loaded:!1};return r[n].call(t.exports,t,t.exports,e),t.loaded=!0,t.exports}var o={};return e.m=r,e.c=o,e.p="",e(0)}([function(r,e,o){"use strict";var n=o(1),t=o(4);_rollbarConfig=_rollbarConfig||{},_rollbarConfig.rollbarJsUrl=_rollbarConfig.rollbarJsUrl||"https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.4.6/rollbar.min.js",_rollbarConfig.async=void 0===_rollbarConfig.async||_rollbarConfig.async;var a=n.setupShim(window,_rollbarConfig),l=t(_rollbarConfig);window.rollbar=n.Rollbar,a.loadFull(window,document,!_rollbarConfig.async,_rollbarConfig,l)},function(r,e,o){"use strict";function n(r){return function(){try{return r.apply(this,arguments)}catch(r){try{console.error("[Rollbar]: Internal error",r)}catch(r){}}}}function t(r,e){this.options=r,this._rollbarOldOnError=null;var o=s++;this.shimId=function(){return o},"undefined"!=typeof window&&window._rollbarShims&&(window._rollbarShims[o]={handler:e,messages:[]})}function a(r,e){if(r){var o=e.globalAlias||"Rollbar";if("object"==typeof r[o])return r[o];r._rollbarShims={},r._rollbarWrappedError=null;var t=new p(e);return n(function(){e.captureUncaught&&(t._rollbarOldOnError=r.onerror,i.captureUncaughtExceptions(r,t,!0),i.wrapGlobals(r,t,!0)),e.captureUnhandledRejections&&i.captureUnhandledRejections(r,t,!0);var n=e.autoInstrument;return e.enabled!==!1&&(void 0===n||n===!0||"object"==typeof n&&n.network)&&r.addEventListener&&(r.addEventListener("load",t.captureLoad.bind(t)),r.addEventListener("DOMContentLoaded",t.captureDomContentLoaded.bind(t))),r[o]=t,t})()}}function l(r){return n(function(){var e=this,o=Array.prototype.slice.call(arguments,0),n={shim:e,method:r,args:o,ts:new Date};window._rollbarShims[this.shimId()].messages.push(n)})}var i=o(2),s=0,d=o(3),c=function(r,e){return new t(r,e)},p=function(r){return new d(c,r)};t.prototype.loadFull=function(r,e,o,t,a){var l=function(){var e;if(void 0===r._rollbarDidLoad){e=new Error("rollbar.js did not load");for(var o,n,t,l,i=0;o=r._rollbarShims[i++];)for(o=o.messages||[];n=o.shift();)for(t=n.args||[],i=0;i<t.length;++i)if(l=t[i],"function"==typeof l){l(e);break}}"function"==typeof a&&a(e)},i=!1,s=e.createElement("script"),d=e.getElementsByTagName("script")[0],c=d.parentNode;s.crossOrigin="",s.src=t.rollbarJsUrl,o||(s.async=!0),s.onload=s.onreadystatechange=n(function(){if(!(i||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState)){s.onload=s.onreadystatechange=null;try{c.removeChild(s)}catch(r){}i=!0,l()}}),c.insertBefore(s,d)},t.prototype.wrap=function(r,e,o){try{var n;if(n="function"==typeof e?e:function(){return e||{}},"function"!=typeof r)return r;if(r._isWrap)return r;if(!r._rollbar_wrapped&&(r._rollbar_wrapped=function(){o&&"function"==typeof o&&o.apply(this,arguments);try{return r.apply(this,arguments)}catch(o){var e=o;throw e&&("string"==typeof e&&(e=new String(e)),e._rollbarContext=n()||{},e._rollbarContext._wrappedSource=r.toString(),window._rollbarWrappedError=e),e}},r._rollbar_wrapped._isWrap=!0,r.hasOwnProperty))for(var t in r)r.hasOwnProperty(t)&&(r._rollbar_wrapped[t]=r[t]);return r._rollbar_wrapped}catch(e){return r}};for(var u="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,captureEvent,captureDomContentLoaded,captureLoad".split(","),f=0;f<u.length;++f)t.prototype[u[f]]=l(u[f]);r.exports={setupShim:a,Rollbar:p}},function(r,e){"use strict";function o(r,e,o){if(r){var t;if("function"==typeof e._rollbarOldOnError)t=e._rollbarOldOnError;else if(r.onerror){for(t=r.onerror;t._rollbarOldOnError;)t=t._rollbarOldOnError;e._rollbarOldOnError=t}var a=function(){var o=Array.prototype.slice.call(arguments,0);n(r,e,t,o)};o&&(a._rollbarOldOnError=t),r.onerror=a}}function n(r,e,o,n){r._rollbarWrappedError&&(n[4]||(n[4]=r._rollbarWrappedError),n[5]||(n[5]=r._rollbarWrappedError._rollbarContext),r._rollbarWrappedError=null),e.handleUncaughtException.apply(e,n),o&&o.apply(r,n)}function t(r,e,o){if(r){"function"==typeof r._rollbarURH&&r._rollbarURH.belongsToShim&&r.removeEventListener("unhandledrejection",r._rollbarURH);var n=function(r){var o,n,t;try{o=r.reason}catch(r){o=void 0}try{n=r.promise}catch(r){n="[unhandledrejection] error getting `promise` from event"}try{t=r.detail,!o&&t&&(o=t.reason,n=t.promise)}catch(r){t="[unhandledrejection] error getting `detail` from event"}o||(o="[unhandledrejection] error getting `reason` from event"),e&&e.handleUnhandledRejection&&e.handleUnhandledRejection(o,n)};n.belongsToShim=o,r._rollbarURH=n,r.addEventListener("unhandledrejection",n)}}function a(r,e,o){if(r){var n,t,a="EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(",");for(n=0;n<a.length;++n)t=a[n],r[t]&&r[t].prototype&&l(e,r[t].prototype,o)}}function l(r,e,o){if(e.hasOwnProperty&&e.hasOwnProperty("addEventListener")){for(var n=e.addEventListener;n._rollbarOldAdd&&n.belongsToShim;)n=n._rollbarOldAdd;var t=function(e,o,t){n.call(this,e,r.wrap(o),t)};t._rollbarOldAdd=n,t.belongsToShim=o,e.addEventListener=t;for(var a=e.removeEventListener;a._rollbarOldRemove&&a.belongsToShim;)a=a._rollbarOldRemove;var l=function(r,e,o){a.call(this,r,e&&e._rollbar_wrapped||e,o)};l._rollbarOldRemove=a,l.belongsToShim=o,e.removeEventListener=l}}r.exports={captureUncaughtExceptions:o,captureUnhandledRejections:t,wrapGlobals:a}},function(r,e){"use strict";function o(r,e){this.impl=r(e,this),this.options=e,n(o.prototype)}function n(r){for(var e=function(r){return function(){var e=Array.prototype.slice.call(arguments,0);if(this.impl[r])return this.impl[r].apply(this.impl,e)}},o="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId,captureEvent,captureDomContentLoaded,captureLoad".split(","),n=0;n<o.length;n++)r[o[n]]=e(o[n])}o.prototype._swapAndProcessMessages=function(r,e){this.impl=r(this.options);for(var o,n,t;o=e.shift();)n=o.method,t=o.args,this[n]&&"function"==typeof this[n]&&("captureDomContentLoaded"===n||"captureLoad"===n?this[n].apply(this,[t[0],o.ts]):this[n].apply(this,t));return this},r.exports=o},function(r,e){"use strict";r.exports=function(r){return function(e){if(!e&&!window._rollbarInitialized){r=r||{};for(var o,n,t=r.globalAlias||"Rollbar",a=window.rollbar,l=function(r){return new a(r)},i=0;o=window._rollbarShims[i++];)n||(n=o.handler),o.handler._swapAndProcessMessages(l,o.messages);window[t]=n,window._rollbarInitialized=!0}}}}]);
// End Rollbar Snippet
</script>

<script>
/* JS scrolling */
!function(t){function e(i){if(n[i])return n[i].exports;var r=n[i]={i:i,l:!1,exports:{}};return t[i].call(r.exports,r,r.exports,e),r.l=!0,r.exports}var n={};e.m=t,e.c=n,e.i=function(t){return t},e.d=function(t,n,i){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:i})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="",e(e.s=3)}([/*!**********************!*\
  !*** ./index.coffee ***!
  \**********************/
function(t,e,n){var i,r,o,s,c,h,u=function(t,e){return function(){return t.apply(e,arguments)}};c=n(/*! utils/throttle */2),o=n(/*! utils/debounce */1),h=function(t,e){if(mamka)return mamka("send_event",{name:t,meta:e})},s=function(){return(new Date).getTime()},i=function(){function t(t){this.reach=u(this.reach,this),this.check=u(this.check,this),this.create=u(this.create,this),this.container=t,this.type="blog_wp",this.create(),window.addEventListener("load",this.create),window.addEventListener("resize",o(this.create,400)),window.addEventListener("scroll",c(this.check,400))}return t.prototype.parts=5,t.prototype.next=0,t.prototype.time={current:s(),loaded:s()},t.prototype.create=function(){var t,e,n,i,r,o,s;for(this.height=this.container.getBoundingClientRect().height,s=this.container.offsetTop,o=[s],t=e=1,i=this.parts+1;1<=i?e<i:e>i;t=1<=i?++e:--e)n=o[t-1],r=parseInt(n+this.height/this.parts,10),o.push(r);return this.stops=o},t.prototype.check=function(){var t,e;return t=window.scrollY,e=t+window.innerHeight,t<=this.stops[0]?this.next=0:e>=this.stops[this.next]&&(this.stops[this.next]<this.stops[this.next+1]||!this.stops[this.next+1])?this.reach(this.next+1,e):void 0},t.prototype.reach=function(t,e){var n;return n={diff:{prev:s()-this.time.current,loaded:s()-this.time.loaded},next:this.next,type:this.type,top:e,height:this.height},h("page--scroll--reachBreakpoint",n),this.time.current=s(),this.next=t},t}(),(r=document.querySelector(".single .main"))&&new i(r)},/*!*******************************!*\
  !*** ./utils/debounce.coffee ***!
  \*******************************/
function(t,e){var n=[].slice;t.exports=function(t,e,i){var r;return r=void 0,function(){var o,s,c,h;if(o=1<=arguments.length?n.call(arguments,0):[],c=this,h=function(){if(r=null,!i)return t.apply(c,o)},s=i&&!r,clearTimeout(r),r=setTimeout(h,e),s)return t.apply(c,o)}}},/*!*******************************!*\
  !*** ./utils/throttle.coffee ***!
  \*******************************/
function(t,e){t.exports=function(t,e,n){var i,r;return null==e&&(e=250),null==n&&(n=this),r=void 0,i=void 0,function(){var o,s;return s=+new Date,o=arguments,r&&s<r+e?(clearTimeout(i),i=setTimeout(function(){return r=s,t.apply(n,o)},e)):(r=s,t.apply(n,o))}}},/*!****************************!*\
  !*** multi ./index.coffee ***!
  \****************************/
function(t,e,n){t.exports=n(/*! /Users/akolmakov/Projects/scroll/index.coffee */0)}]);
</script>
</body>
</html>
