(function() {
    var types = [
        {
            name: 'восклицательный знак - голубой',
            class: 'main'
        },
        {
            name: 'восклицательный знак - оранжевый',
            class: 'main-orange'
        },
        {
            name: 'восклицательный знак - серый',
            class: 'main-grey'
        },
        {
            name: 'вопросительный - голубой',
            class: 'question'
        },
        {
            name: 'вопросительный - оранжевый',
            class: 'question-orange'
        },
        {
            name: 'вопросительный - серый',
            class: 'question-grey'
        },
        {
            name: 'звездочка - голубой',
            class: 'star'
        },
        {
            name: 'звездочка - оранжевый',
            class: 'star-orange'
        },
        {
            name: 'звездочка - серый',
            class: 'star-grey'
        },
        {
            name: 'молния - голубой',
            class: 'light'
        },
        {
            name: 'молния - оранжевый',
            class: 'light-orange'
        },
        {
            name: 'молния - серый',
            class: 'light-grey'
        },
        {
            name: 'бабл речевой - голубой',
            class: 'quote'
        },
        {
            name: 'бабл речевой - оранжевый',
            class: 'quote-orange'
        },
        {
            name: 'бабл речевой - серый',
            class: 'quote-grey'
        },
        {
            name: 'Кнопка поиска билетов'
        },
        {
            name: 'Кнопка поиска отелей'
        },
        {
            name: 'Вставка "Читай также"'
        },
        {
            name: 'Цитата'
        }
    ];

    tinymce.create('tinymce.plugins.avs', {
        init: function(ed, url) {
            ed.addButton('dropcap', {
                title : 'Врезка',
                text: 'Важная врезка',
                type: 'listbox',
                onselect: function(e) {
                //    console.log(e)
                },
                values: types.map(function (el) {
                    return {
                        text: el.name,
                        code: el.class,
                        onclick: function () {
                            var selection = tinymce.activeEditor.selection;
                            if (el.name === 'Кнопка поиска билетов') {
                                var text = prompt("Текст", "Текст кнопки");
                                selection.setContent('[button href="https://www.aviasales.ru"]' + text + '[/button]');
                            } else if (el.name === 'Кнопка поиска отелей') {
                                var text = prompt("Текст", "Текст кнопки");
                                selection.setContent('[button href="https://hotels.aviasales.ru/"]' + text + '[/button]');
                            } else if (el.name === 'Вставка "Читай также"') {
                                var text = prompt("Текст", "Текст");
                                var link = prompt("URL", "Ссылка");
                                selection.setContent('[readmore text="' + text + '" link="' + link + '"]');
                            } else if (el.name === 'Цитата') {
                                var name = prompt("Имя", "Имя");
                                var description = prompt("Должность", "Должность");
                                selection.setContent('[blockquote name="' + name + '" description="' + description + '" ][/blockquote]');
                            } else {
                                selection.setContent('<div class="important important--' + el.class + '">' + selection.getContent() + '</div>');
                            }
                        }
                    }
                })
            });
        }
    });

    // Register plugin
    tinymce.PluginManager.add('avs', tinymce.plugins.avs);
})();

//
// editor.addButton('shortcodes', {
//             type: 'listbox',
//             text: 'Shortcodes',
//             icon: false,
//             onselect: function(e) {},
//             values: [
//
//                 {
//                     text: 'H1 Title',
//                     onclick: function() {
//                         tinymce.execCommand('mceInsertContent', false, '[h1][/h1]');
//                     }
//                 }, {
//                     text: 'H2 Title',
//                     onclick: function() {
//
//                         var selected2 = false;
//                         var content2 = selected2 = tinyMCE.activeEditor.selection.getContent();
//                         var h2titleclass = prompt("Would you like a custom class?", "");
//
//                         if (h2titleclass != '') {
//                             h2titleclass = ' class= "' + h2titleclass + '"';
//                         }
//
//                         if (selected2 !== '') {
//                             content2 = '[h2' + h2titleclass + ']' + selected2 + '[/h2]';
//                         } else {
//                             content2 = '[h2' + h2titleclass + '][/h2]';
//                         }
//
//                         tinymce.execCommand('mceInsertContent', false, content2);
//                     }
//                 }
//             ]
//
//         });
