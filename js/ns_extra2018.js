document.addEventListener('DOMContentLoaded', function(){
	wrapTables();
});

// Wrap all tables in article for better responsiveness
function wrapTables() {
	var tables = document.querySelectorAll('.main article table'),
        counter = tables.length,
        wrap;

    while(counter) {
        counter--;
        wrap = document.createElement('div');
        wrap.classList.add('table-wrap');
        tables[counter].parentNode.insertBefore(wrap, tables[counter]);
        wrap.appendChild(tables[counter]);
    }
}
