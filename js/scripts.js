function initPhotoSwipe() {
    var pswpElement = document.querySelectorAll('.pswp')[0]

    var options = {
        index: 0, // start at first slide
        history: false,
        focus: false,
        shareEl: false
    };

    var list = document.querySelector('.gallery')
    var items = []

    if (list) {
        var photos = list.querySelectorAll('a')
        for (var i = 0; i < photos.length; i++) {
            var photo = photos[i].querySelector('img')
            var index = i
            items.push({
                src: photo.getAttribute('src'),
                w: parseInt(photo.getAttribute('width'), 10),
                h: parseInt(photo.getAttribute('height'), 10)
            })
            photos[i].addEventListener('click', function(e) {
                e.preventDefault()
                options.index = index
                var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init()
            })

        }
    }
}

function initSocials() {
    var socials = document.querySelectorAll('.socials__item[data-code]');
    for (var i = 0; i < socials.length; i++) {
        var link = socials[i];
        var url = link.dataset.url;
        var code = Math.random().toString(36).slice(2, 8);
        link.setAttribute('href', link.getAttribute('href').split('url=')[0] + 'url=' + encodeURIComponent(url + '&code=' + code))
    }

    var socialCountUrl = document.querySelector('.socials__base-url');
    if (socialCountUrl) {
        var setSocialCount = function(provider, count) {
            if (count > 0) {
                var items = document.querySelectorAll(`.socials__count--${provider}`);
                for (var i = 0; i < items.length; i++) {
                    var item = items[i]
                    if (!item.dataset.count) {
                        item.dataset.count = count
                    }
                    if (count >= parseInt(item.dataset.count, 10)) {
                        item.innerHTML = count;
                        item.classList.add('socials__count--active');
                    }
                }
            }
        }
        var providers = ['vk', 'facebook', 'ok']
        var callback = function (provider) {
            return function (count) {
                setSocialCount(provider, count);
            }
        }
        for (var i = 0; i < providers.length; i++) {
            var provider = providers[i];
            var link = document.querySelector('.socials__item--' + provider)
            if (link) {
                var shareUrl = provider === 'facebook' ? socialCountUrl.dataset.url : link.dataset.url;
                socialCount.get(provider, (provider === 'ok') ? encodeURIComponent(shareUrl) : shareUrl, callback(provider))
                shareUrl = shareUrl.replace('www.aviasales.ru/blog', 'blog.aviasales.ru')
                socialCount.get(provider, (provider === 'ok') ? encodeURIComponent(shareUrl) : shareUrl, callback(provider))
            }
        }
    }
}

function initForms() {
    var form = document.getElementById('subscribe-blog-blog_subscription-2')
    var input = document.getElementById('subscribe-field-blog_subscription-2')
    var handler = function(event) {
        if (event.preventDefault) event.preventDefault();
        if (!/[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+/.test(input.value)) {
            input.focus();
        } else {
            jQuery.ajax({
                url: (form.dataset.action || '/wp-content/themes/aviasales-v2') + '/subscription.php',
                type: 'POST',
                data: {
                    email: input.value
                },
                success: function(response) {
                    if (response === 'null')
                        console.log('something wrong with subscription');
                    var hrSubscribe = jQuery('.header-right__subscribe')
                    hrSubscribe.html('Ура, вы подписались!');
                    hrSubscribe.toggleClass('header-right__subscribe_active', false);
                    hrSubscribe.toggleClass('header-right__subscribe_finish', true);
                },
                error: function(xhr, status, error) {
                    console.log('subscription error: ' + status);
                }
            })
        }
    };
    if (form) form.addEventListener('submit', handler, false);

    var emailForm = document.querySelector('.news-subscription__form')

    if (emailForm) {
        emailForm.addEventListener('submit', function (e) {
            e.preventDefault()
            var input = document.querySelector('.news-subscription__email-input')
            var block = document.querySelector('.news-subscription')

            block.classList.remove('is-error')

            if (!/[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+/.test(input.value)) {
                block.classList.add('is-error')
            } else {
                block.classList.add('is-show-loader')
                jQuery.ajax({
                    url: 'blog/wp-content/themes/aviasales-v2/subscription.php', //TODO: убрать захардкоженный урл
                    type: 'POST',
                    data: { email: input.value },
                    success: function (response) {
                        block.classList.add('is-done')
                    }
                })
            }
        }, false);
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    initPhotoSwipe()
    initSocials()
    initForms()
});

(function($, root, undefined) {
// jquery pragrammiravanie
$(function() {
    'use strict';

    var ACTIVE_MENU_CLASS = 'navbar__menu_active';
    var $navbarMenu = $('.navbar__menu');
    var $navbarTrigger = $('.hamburger', $navbarMenu);

    $navbarTrigger.click(function navbarTriggerClick() {
        $navbarMenu.toggleClass(ACTIVE_MENU_CLASS);
    });

    $(document.body).focus(function handleBodyFocus(e) {
        $navbarMenu.toggleClass(ACTIVE_MENU_CLASS, !$navbarMenu.is(e.target) && !$navbarMenu.has(e.target));
    });

    var LANGUAGE_HIDDEN_CLASS = 'is-hidden';
    var LANGUAGE_TRIGGER_ACTIVE_CLASS = '--is-opened';

    var $languageSelector = $('.footer__arrow-link');
    var $languagesContainer = $('.footer__country-list');
    $languageSelector.on('click', function() {
        $languageSelector.toggleClass(LANGUAGE_TRIGGER_ACTIVE_CLASS);
        $languagesContainer.toggleClass(LANGUAGE_HIDDEN_CLASS);
    });

    var SUBSCRIBE_ACTIVE_CLASS = 'header-right__subscribe_active';
    var $subscribeWrapper = $('.header-right__subscribe');

    $subscribeWrapper.one('click', function subscribeWrapperClicked(e) {
        $subscribeWrapper.toggleClass(SUBSCRIBE_ACTIVE_CLASS, true);
    });

    var $goTopButton = $('.go-to-top-button');
    var GO_TOP_HIDDEN_CLASS = '--is-hidden';

    $goTopButton.on('click', function() {
        $goTopButton.addClass(GO_TOP_HIDDEN_CLASS);
        $('html, body').animate({
            scrollTop: '0px'
        }, 300);
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() < $(window).height() * 0.35) {
            $goTopButton.addClass(GO_TOP_HIDDEN_CLASS);
        } else {
            $goTopButton.removeClass(GO_TOP_HIDDEN_CLASS);
        }
    });

    function getStikyParams() {
        var params = {
            contentPosition: $('.header__content').offset().top,
            toolbarHeight: $('.header__links').height(),
            containerHeight: $('.header__container').height(),
            containerDiff: function() {
                return this.containerHeight - this.toolbarHeight
            },
            contentDiff: function() {
                return this.contentPosition - this.toolbarHeight
            }
        }

        $('.header').css('min-height', params.containerHeight + 'px');

        return params;
    }

    function setSticky(contentDiff, containerDiff) {
        if ($(window).scrollTop() > contentDiff) {
            $('.header__content').addClass('--is-sticky');
        } else {
            $('.header__content').removeClass('--is-sticky');
        }

        if ($(window).scrollTop() > containerDiff) {
            $('.header').addClass('--is-sticky');
            $('.header__links').addClass('--is-sticky');
        } else {
            $('.header').removeClass('--is-sticky');
            $('.header__links').removeClass('--is-sticky');
        }
    }

    var stickyParams;

    stickyParams = getStikyParams();
    setSticky(stickyParams.contentDiff(), stickyParams.containerDiff())

    $(window).resize(function() {
        stickyParams = getStikyParams();
    });

    $(window).scroll(function(event) {
        setSticky(stickyParams.contentDiff(), stickyParams.containerDiff())
    });

});
})(jQuery, this);

function getMarker() {
    var refMarker  = location.search.match(/marker=(.*?)(;|&|$|\s)/) || {};
    var marker = refMarker[1] === undefined ? false : refMarker[1];
    return marker;
}

function setCookie(name, value, options) {
    options = options || {};
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
}

function getDomain() {
    var hostname = location.hostname
    var parts = hostname.split('.')
    var parts = parts.slice((parts.length - 2), parts.length)
    return parts.join('.')
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setMarker() {
    var marker = getMarker();

    if (marker) {
        var exdate = new Date()
        exdate.setDate(exdate.getDate() + (10 * 365))

        var options = {
            domain: '.' + getDomain(),
            path: '/',
            expires: exdate.toUTCString()
        }

        setCookie('marker', marker, options);
    }
}

function initSearchWidget() {
    var FORM_ID = 'a8e04b3e13f2179ceee70ea3a61b624e';
    var marker = getCookie('marker') ? getCookie('marker') : 143413;
    var container = document.querySelector('.aviasales-widget');

    var settings = {
        "handle": "a8e04b3e13f2179ceee70ea3a61b624e",
        "widget_name": "Поисковая форма Блога",
        "border_radius": "2",
        "additional_marker": null,
        "width": null,
        "show_logo": true,
        "show_hotels": false,
        "form_type": "avia",
        "locale": "ru",
        "currency": "rub",
        "sizes": "default",
        "search_target": "_blank",
        "active_tab": "avia",
        "search_host": "aviasales.ru/search",
        "hotels_host": "search.hotellook.com",
        "hotel": "",
        "hotel_alt": "",
        "avia_alt": "",
        "retargeting": true,
        "trip_class": "economy",
        "depart_date": null,
        "return_date": null,
        "check_in_date": null,
        "check_out_date": null,
        "no_track": false,
        "id": 148600,
        "marker": marker,
        "origin": {
            "name": ""
        },
        "destination": {
            "name": ""
        },
        "color_scheme": {
            "name": "custom",
            "icons": "icons_white",
            "background": "#00b0dd",
            "color": "#ffffff",
            "border_color": "transparent",
            "button": "#ff6d00",
            "button_text_color": "#ffffff",
            "input_border": "#007897"
        },
        "hotels_type": "hotellook_host",
        "best_offer": {
            "locale": "ru",
            "currency": "rub",
            "marker": marker,
            "search_host": "aviasales.ru/search",
            "offers_switch": false,
            "api_url": "//www.travelpayouts.com/minimal_prices/offers.json",
            "routes": []
        },
        "hotel_logo_host": null,
        "search_logo_host": "www.aviasales.ru",
        "hotel_marker_format": null,
        "hotelscombined_marker": null,
        "responsive": true,
        "height": 182
    }

    window.TP_FORM_SETTINGS = window.TP_FORM_SETTINGS || {};
    window.TP_FORM_SETTINGS[FORM_ID] = settings;

    var script = document.createElement('script');
    script.src = '//www.travelpayouts.com/widgets/' + FORM_ID + '.js?v=1406';
    script.async = true;

    container.appendChild(script);
}

function replaceMarker() {
    var article = document.querySelector('.article');
    var buttons = article ? article.querySelectorAll('.of_main_form__submit.text-button') : false;

    if (buttons) {
        for (let i = 0; i < buttons.length; i += 1) {
            var btn = buttons[i];
            var href = btn.getAttribute('href');
            var refMarker = href.match(/marker=(.*?)(;|&|$|\s)/) || {}
            var markerString = refMarker[0] === undefined ? false : refMarker[0];

            if (markerString) {
                var replacedHref = href.replace(('?' + markerString), '');
                btn.setAttribute('href', replacedHref);
            }
        }
    }
}

setMarker();
initSearchWidget();

document.addEventListener('DOMContentLoaded', function () {
    replaceMarker(); //TODO: убрать этот костыль и удалить маркеры из ссылок в БД
});