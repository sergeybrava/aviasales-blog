<!-- article -->
<article id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>
    <a class="post-excerpt__container" href="<?php the_permalink(); ?>">
        <div class="post-excerpt__image-wrap">
            <?php
                $image = getImageSrcFromContent();
                if (!empty($image)) {
                    $file = get_attachment_id(basename($image));
                    $url = wp_get_attachment_image_src($file, 'post-thumbnail');
                    $imageUrl = $url ? $url[0] : $image;
            ?>
                <div class="post-excerpt__image" style="background-image:url(<?php echo $imageUrl ?>)"></div>
            <?php } ?>
        </div>
        <?php
          $categories = get_the_category();
          if (!empty($categories)) {
        ?>
            <div class="post-excerpt__category post-excerpt__category--<?php echo $categories[0]->term_id; ?>"><?php echo esc_html($categories[0]->name); ?></div>
        <?php } ?>
        <div class="post-excerpt__content">
            <h2 class="post-excerpt__title"><?php the_title(); ?></h2>
            <div class="post-excerpt__author"><?php the_author(); ?></div>
        </div>
    </a>
</article>
<!-- /article -->
