<?php
  global $post;
  $categories = wp_get_post_categories($post->ID);
  get_header();
?>
<div class="socials">
  <div class="socials__inner">
    <?php get_template_part( 'socials' ); ?>
  </div>
</div>
<div class="breadcrumbs">
  <ul class="breadcrumbs__list">
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/">Дешевые авиабилеты</a>
    </li>
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="/blog">Блог</a>
    </li>
    <?php if ( !empty($categories) ): ?>
    <li class="breadcrumbs__item">
      <a class="breadcrumbs__link" href="<?php
        $url = get_category_link($categories[0]);
        if(substr($url, -1) == '/') {
          $url = substr($url, 0, -1);
        }
        echo esc_url($url);
      ?>">
        <?php
          echo esc_html(get_cat_name($categories[0]));
        ?>
      </a>
    </li>
    <?php endif; ?>
    <li class="breadcrumbs__item">
      <span class="breadcrumbs__link breadcrumbs__link_disabled"><?php echo $post->post_title; ?></span>
    </li>
  </ul>
</div>
<?php while ( have_posts() ) : the_post(); ?>
    
<section class="main">
  <article class="article">
	<?php get_template_part( 'content-single', get_post_format() ); ?>
  </article>
</section>

<?= do_shortcode('[as_subscribe_form]') ?>

<div class="socials social--bottom">
  <div class="socials__inner">
    <?php get_template_part( 'socials' ); ?>
  </div>
</div>
<div class="disqus">
  <div class="disqus__inner">
    <?php comments_template( '', true ); ?>
  </div>
</div>
<?php dynamic_sidebar('after-post') ?>

<?php endwhile; // end of the loop. ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
