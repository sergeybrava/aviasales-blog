<div class="news-subscription " is="news_subscription">
    <div class="news-subscription__inner-wrap">
        <div class="news-subscription__balloon-icon"></div>
        <div class="news-subscription__form-wrap">
            <div class="news-subscription__title">Новостная рассылка</div>
            <div class="news-subscription__description">Новости, скидки, распродажи, конкурсы и&nbsp;немного искусства:</div>
            <div class="news-subscription__error">Некорректный email. Попробуйте еще раз!</div>
            <form action="/" class="news-subscription__form" data-goal-category="subscribeNews" data-role="news_form" method="get">
                <input autocapitalize="off" autocorrect="off" class="news-subscription__email-input" name="email" placeholder="Адрес электронной почты" spellcheck="false" type="text">
                <input
                    class="news-subscription__submit-button submit-button --orange" type="submit" value="Подписаться">
                    <div class="news-subscription__spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
            </form>
            <div class="news-subscription__remark">Нажимая &laquo;Подписаться&raquo;, вы&nbsp;соглашаетесь с&nbsp;правилами <a href='https://www.aviasales.ru/terms-of-use' target='blank' class="news-subscription__remark --blue">использования сервиса</a> и&nbsp;<a href='https://www.aviasales.ru/privacy' target='blank' class="news-subscription__remark --blue">обработки персональных данных.</a></div>
        </div>
        <div class="news-subscription__thanks-wrap">
            <div class="news-subscription__thanks-title">Ура! Теперь ты&nbsp;в&nbsp;курсе<br>самого интересного</div>
            <div class="news-subscription__thanks-description">Обещаем присылать письма только по&nbsp;действительно важному поводу.</div>
        </div>
    </div>
</div>
