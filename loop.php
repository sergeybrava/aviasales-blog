<div class="categories-top-wrapper is-phablet-hidden">
	<?php aviasales_get_categories(); ?>
	<div class="search-form">
		<?php get_search_form(true); ?>
	</div>
</div>
<div class="post-excerpts">
	<div class="post-excerpts__inner">
		<?php
			if (have_posts()):
				$index = 0;
				while (have_posts()) : the_post();
					$index++;
					?>
						<?php get_template_part('loop', 'item'); ?>
				<?php
					if ($index === 5): ?>
							</div>
						</div>
						<?php get_template_part('form'); ?>
						<div class="post-excerpts">
							<div class="post-excerpts__inner">
				<?php
					endif;
				endwhile; ?>
		<?php endif; ?>
	</div>
</div>
