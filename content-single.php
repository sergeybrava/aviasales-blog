<?php
	global $post;
	$post_parts = explode('<!--more-->', $post->post_content);
	//echo print_r($post, true);
?>
<?php if (count($post_parts) > 1) : ?>
	<p class="article__teaser"><?php echo trim(strip_tags($post_parts[0])); ?></p>
<?php endif; ?>

<?= do_shortcode( '[quest_form_question]' ); ?>
<p><?= apply_filters('the_content', trim(count($post_parts) > 1 ? $post_parts[1] : $post_parts[0])); ?></p>
<?= do_shortcode( '[quest_form_answer]' ); ?>
