<?php
/*
 *  Author: zaycker | @zaycker
 *  URL: zaycker.ru
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Theme Support
\*------------------------------------*/
function aviasales_setup()
{
  if (function_exists('add_theme_support'))
  {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('posthumbnails');
    set_post_thumbnail_size(640, 400, true);
    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');
  }
}
/*------------------------------------*\
    Functions
\*------------------------------------*/

function get_attachment_id( $url ) {
    $attachment_id = 0;
    $dir = wp_upload_dir();

    $file = basename( $url );
    $query_args = array(
        'post_type'   => 'attachment',
        'post_status' => 'inherit',
        'fields'      => 'ids',
        'meta_query'  => array(
            array(
                'value'   => $file,
                'compare' => 'LIKE',
                'key'     => '_wp_attachment_metadata',
            ),
        )
    );
    $query = new WP_Query( $query_args );
    if ( $query->have_posts() ) {
        foreach ( $query->posts as $post_id ) {
            $meta = wp_get_attachment_metadata( $post_id );
            $original_file       = basename( $meta['file'] );
            $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
            if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                $attachment_id = $post_id;
                break;
            }
        }
    }

    return $attachment_id;
}

function getImageSrcFromContent($content = '')
{
    if (empty($content)) {
        $content = get_the_content();
    }
    preg_match('/<img.+src=\"(.+?)\".*/i', $content, $matches);
    return count($matches) ? $matches[1] : '';
}

function getImageSrcFromSinglePost($post)
{
    if ( has_post_thumbnail($post) ) {
        return getImageSrcFromContent( get_the_post_thumbnail( $post, 'post-thumbnail', '' ) );
    }

    $post_parts = explode('<!--more-->', $post->post_content);
    return count($post_parts) > 1 ? getImageSrcFromContent($post_parts[0]) : '';
}

function getHeaderPost()
{
    global $post;
    if ( is_home() ) {
        $recent_posts = wp_get_recent_posts( array(
            'numberposts' => 1,
            'post_status' => 'publish',
        ), OBJECT);

        $post = array_pop( $recent_posts );
    }

    return $post;
}

function aviasales_nav()
{
    wp_nav_menu(
    array(
        'theme_location'  => 'header-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul>%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
        )
    );
}

function aviasales_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_register_script('socialcount', get_template_directory_uri() . '/js/lib/socialcount.js', array(), '1.0.1'); // shares-count
        wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js', array(), '0.0.12');
        wp_register_script('ns_extra', get_template_directory_uri() . '/js/ns_extra2018.js', array(), '0.0.13');
        wp_register_script('photoswipe', get_template_directory_uri() . '/photoswipe/photoswipe.min.js', array(), '0.1');
        wp_register_script('photoswipe-ui', get_template_directory_uri() . '/photoswipe/photoswipe-ui-default.min.js', array(), '0.1');
        wp_enqueue_script('socialcount'); // Enqueue it!
        wp_enqueue_script('modernizr'); // Enqueue it!
        wp_enqueue_script('conditionizr'); // Enqueue it!
        wp_enqueue_script('jquery');
        wp_enqueue_script('scripts'); // Enqueue it!
        wp_enqueue_script('ns_extra'); // Enqueue it!
        wp_enqueue_script('photoswipe');
        wp_enqueue_script('photoswipe-ui'); // Enqueue it!
    }
    if (is_admin()) {
        add_editor_style(get_template_directory_uri() . '/style.css?v=1.1');
    }
}

function aviasales_styles()
{
    wp_register_style('photoswipe', get_template_directory_uri() . '/photoswipe/photoswipe.css', array(), '1.1', 'all');
    wp_enqueue_style('photoswipe'); // Enqueue it!

    wp_register_style('default-skin', get_template_directory_uri() . '/photoswipe/default-skin/default-skin.css', array(), '1.1', 'all');
    wp_enqueue_style('default-skin'); // Enqueue it!

    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.1', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('aviasales_header', get_template_directory_uri() . '/header.css', array(), '1.45', 'all');
    wp_enqueue_style('aviasales_header'); // Enqueue it!

    wp_register_style('aviasales_nesutulsa', get_template_directory_uri() . '/css/ns_extra2018.css', array(), '1.43', 'all');
    wp_enqueue_style('aviasales_nesutulsa'); // Enqueue it!
}

function aviasales_footer_styles()
{
    wp_register_style('aviasales_footer', get_template_directory_uri() . '/style.css', array(), '1.44', 'all');
    wp_enqueue_style('aviasales_footer'); // Enqueue it!
}


// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function aviasales_pagination()
{
    global $wp_query;
    $big = 999999999;
    $pages = paginate_links(
        array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
            'prev_text' => '',
            'next_text' => ''
        )
    );

    $pages = preg_replace('/blog\/page\/(\d+)\//', 'blog/page/$1', $pages);

    if($pages) {
        if (!strpos($pages, 'prev')) {
            $pages = '<span class="prev page-numbers disable"></span>'.$pages;
        }
        if (!strpos($pages, 'next')) {
            $pages .= '<span class="next page-numbers disable"></span>';
        }
    }

    echo $pages;
}

function aviasales_view_article($more)
{
    return '';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function aviasales_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function aviasalesgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function aviasalescomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
    <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php }

function aviasales_widgets_init()
{
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'twentytwelve' ),
        'id' => 'sidebar-1',
        'description' => __( 'Place for subscription. Nothing more.', 'twentytwelve' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Breadcrumbs', 'aviasales' ),
        'id' => 'header-1',
        'description' => __( 'For all pages header for breadcrumbs', 'aviasales' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer', 'aviasales' ),
        'id' => 'footer',
        'description' => __( 'Footer of every page', 'aviasales' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'After post', 'aviasales' ),
        'id' => 'after-post',
        'description' => __( 'Place after single post', 'aviasales' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'After 3 teasers', 'aviasales' ),
        'id' => 'after-3-teasers',
        'description' => __( 'Place after 3 post teasers', 'aviasales' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'After 6 teasers', 'aviasales' ),
        'id' => 'after-6-teasers',
        'description' => __( 'Place after 6 post teasers', 'aviasales' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="hidden">',
        'after_title' => '</div>',
    ) );
}

function aviasales_get_categories()
{
  $active_class = ' categories__item_active';
  $tpl = '<li class="categories__item%3$s" ><a href="%1$s">%2$s</a></li>';
  $tpl2 = '<li class="categories__item%3$s post-excerpt__category--%4$s" ><a href="%1$s">%2$s</a></li>';
  $categories = get_categories(array(
      'orderby' => 'count',
      'order' => 'DESC',
      'parent'  => 0
  ));
  echo '<ul class="categories">';
  printf($tpl, home_url( '/' ), 'Все записи', is_home() ? $active_class : '');
  foreach ( $categories as $category ) {
      $url = get_category_link($category->term_id);
      if(substr($url, -1) == '/') {
        $url = substr($url, 0, -1);
      }
      printf($tpl2,
          esc_url($url),
          esc_html($category->name),
          is_category($category->cat_ID) ? $active_class : '',
          esc_html($category->cat_ID)
      );
  }
  echo '</ul>';
}

function aviasales_wp_title_for_home( $title )
{
    if ( empty( $title ) && ( is_home() || is_front_page() ) ) {
        return get_bloginfo( 'description' );
    }

    return $title;
}

function aviasales_related()
{
    global $post;
    $custom_taxterms = wp_get_object_terms( $post->ID, 'category', array('fields' => 'ids') );

    $args = array(
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array ($post->ID),
    );

    $related_items = new WP_Query( $args );

    if ($related_items->have_posts()) :
        while ( $related_items->have_posts() ) : $related_items->the_post();
            get_template_part('loop', 'item');
        endwhile;
    endif;

    wp_reset_postdata();
}

function aviasales_get_widget_url($type = 0) {
    $types = array(
        '//www.travelpayouts.com/widgets/d3a6223c8a4b4ef8d3f58cf5384d0d24.js?v=907', // blue
        '//www.travelpayouts.com/widgets/7e65a1675fdc29c508f763fdad6f5ce2.js?v=907', // green
    );

    return $types[$type];
}



function rss_campaign_tracking($post_permalink) {
    $source = 'rss';
    if ($_SERVER['REQUEST_URI'] === '/feed/zen/') {
        $source = 'dzen';
    }
    return $post_permalink . '?utm_source='.$source.'&amp;utm_medium=referral&amp;utm_campaign=blog_'.pathinfo($post_permalink, PATHINFO_BASENAME);
}

function add_shortcode_quote($atts, $content = '') {
    $result = '
        <blockquote class="text-quote">
            <div class="text-quote__content">
                '.$content.'
            </div>';

    if (strlen($atts['image']) > 0) {
        $result .= '<div class="text-quote__image" style="background-image: url('.$atts['image'].')"></div>';
    }

    if (strlen($atts['name']) > 0 || strlen($atts['description']) > 0) {
        $result .= '<div class="text-quote__author">';
        if (strlen($atts['name']) > 0) {
            $result .= '<div class="text-quote__name">';
            if (strlen($atts['link']) > 0) {
                $result .= '<a href="'.$atts['link'].'" target="_blank">';
            }
            $result .= $atts['name'];
            if (strlen($atts['link']) > 0) {
                $result .= '</a>';
            }
            $result .= '</div>';
        }
        if (strlen($atts['description']) > 0) {
            $result .= '<div class="text-quote__description">';
            $result .= $atts['description'];
            $result .= '</div>';
        }
        $result .= '</div>';
    }

    $result .= '</blockquote>';

    return $result;
}

function add_shortcode_button($atts, $content = '') {
    return '
        <div style="text-align: center;">
            <a href="'.strip_tags(str_replace("’", "", $atts['href'])).'" class="of_main_form__submit text-button" target="_blank">
                '.strip_tags($content).'
            </a>
        </div>
    ';
}

function callback($buffer) {
  preg_match_all('/<link>(.*?)<\/link>/', $buffer, $matches);

  for ($i = 0; $i < count($matches[1]); $i++) {
    $path = $matches[1][$i];
    $buffer = str_replace(
        $path.'</link>',
        $path.'?utm_source=facebook_instant&amp;utm_medium=referral&amp;utm_campaign=blog_'.basename($path).'</link>',
        $buffer
    );
  }

  return $buffer;
}

function add_shortcode_readmore($atts, $content = '') {
    $params = shortcode_atts( array(
      'text' => '',
      'title' => 'Читайте также:',
      'link' =>''
    ), $atts);
    return '
        <div class="readmore">
          <div class="readmore__title" >
          '.strip_tags($params['title']).'
          </div>
          <a href="'.strip_tags(str_replace("’", "", $params['link'])).'" class="readmore__text" target="_blank">
                '.strip_tags($params['text']).'
          </a>
        </div>
    ';
}

function buffer_start() {
    ob_start("callback");
}

function buffer_end() {
    ob_end_flush();
}

add_action('wpna_facebook_channel_header', 'buffer_start');
add_action('wpna_facebook_channel_footer', 'buffer_end');

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

add_shortcode('blockquote', 'add_shortcode_quote');
add_shortcode('button', 'add_shortcode_button');
add_shortcode('readmore', 'add_shortcode_readmore');

// Add Actions
add_action('after_setup_theme', 'aviasales_setup');
add_action('init', 'aviasales_header_scripts'); // Add Custom Scripts to wp_head
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'aviasales_styles'); // Add Theme Stylesheet
add_action('get_footer', 'aviasales_footer_styles'); // Add Theme Stylesheet
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action( 'widgets_init', 'aviasales_widgets_init' );
add_action('init', 'aviasales_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'aviasalesgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('excerpt_more', 'aviasales_view_article');
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('style_loader_tag', 'aviasales_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter('wp_title', 'aviasales_wp_title_for_home' );
add_filter('the_permalink_rss', 'rss_campaign_tracking');
add_filter('the_permalink_rss', 'rss_campaign_tracking');
add_filter('wpseo_xml_sitemap_img', '__return_empty_array');
add_filter('wpseo_sitemap_urlimages', '__return_empty_array');

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

/*------------------------------------*\
    ShortCode Functions
\*------------------------------------*/


add_action( 'init', 'avs_buttons' );
function avs_buttons() {
    add_filter("mce_external_plugins", "avs_add_buttons" );
    add_filter('mce_buttons', 'avs_register_buttons' );
}
function avs_add_buttons( $plugin_array ) {
    $plugin_array['avs'] = get_template_directory_uri() . '/js/tinymce.js?v2';
    return $plugin_array;
}
function avs_register_buttons( $buttons ) {
    array_push( $buttons, 'dropcap', 'showrecent' ); // dropcap', 'recentposts
    return $buttons;
}

add_action('wp_loaded', 'map_start');
function map_start() {
    if (strstr($_SERVER['REQUEST_URI'], '-sitemap.xml')) {
        ob_start("xmlhead_replace");
    }
    if (strstr($_SERVER['REQUEST_URI'], '_index.xml')) {
        ob_start("xmlurl_replace");
    }
}

add_action('shutdown', 'map_end');
function map_end()   {
    if (strstr($_SERVER['REQUEST_URI'],'sitemap.xml')) {
        ob_end_flush();
    }
}

function xmlurl_replace($buffer) {
    $buffer = str_replace('www.aviasales.ru/blog', 'blog.aviasales.ru', $buffer);
    return $buffer;
}

function xmlhead_replace($buffer) {
    $buffer = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="//www.aviasales.ru/blog/main-sitemap.xsl"?>', '', $buffer);
    $buffer = str_replace('/</loc>', '</loc>', $buffer);
    return $buffer;
}

function remove_trailing_slash($url) {
    if(substr($url, -1) == '/') {
        $url = substr($url, 0, -1);
    }
    return $url;
}
add_filter('the_permalink', 'remove_trailing_slash');
add_theme_support( 'html5', array( 'search-form' ) );

function prepare_tables_for_ia( $content ) {
    preg_match_all('/(<table.*?>.*?<\/table>)/si', $content, $matches);

    for ($i = 0; $i < count($matches[1]); $i++) {
        $table = $matches[1][$i];
        $content = str_replace(
            $table,
            '<figure class="op-interactive">
                <iframe class="table-wrap">
            ' . $table .
            '
                </iframe>
            </figure>
            ',
            $content
        );
    }

    $replaced_tables = str_replace('<table', '<table style="border-collapse: collapse;border-spacing: 0;width: 100% !important;max-width: 100%;height: auto !important;margin: 20px 0;line-height: 1.7;" ', $content);
    $replaced_td = str_replace('<td', '<td style="padding: 10px;border: 1px solid #b1c3cd;hyphens: auto;" ', $replaced_tables);

    return $replaced_td;
}

function add_shortcode_button_iframe($atts, $content = '') {
    return '<figure class="op-interactive">
        <iframe>
            <div style="text-align: center;">
                <a href="'.strip_tags(str_replace("'", "", $atts['href'])).'" target="_blank" style="font-family: Open Sans,Segoe UI,Arial,Verdana,Tahoma,sans-serif;font-style: normal;font-weight: 600;position: relative;border: 0;box-sizing: border-box;color: #fff;display: inline-block;text-align: center;min-width: 280px;height: 63px;line-height: 3.875rem;font-size: 1.3rem;padding: 0 30px;margin: 5px auto 20px;text-decoration: none;background: #ff6d00;box-shadow: 0 1px 0 0 #d64d08, 0 2px 1px 0 rgba(0,0,0,.1);border-radius: 5px;">
                    '.strip_tags($content).'
                </a>
            </div>
        </iframe>
    </figure>';
}

function add_shortcode_readmore_iframe($atts, $content = '') {
    $params = shortcode_atts( array(
      'text' => '',
      'title' => 'Читайте также:',
      'link' =>''
    ), $atts);

    return '<figure class="op-interactive">
        <iframe>
            <div style="position: relative;border-style: solid;border-color: #FF6D00;border-width: 1px 0 1px 0;margin: 23px 0;padding: 13px 0 13px 30px;">
            <div style="position: absolute;top: -4px;left: 0;width: 18px;height: 38px;background-image: url(//assets.hotellook.com/blog/wp-content/themes/aviasales-v2/./img/imp-readmore.svg);background-repeat: no-repeat;"></div>
                <div style="font-weight: 600;font-size: 20px;line-height: 22px;color: #FF6D00;text-align: left;">
                    '.strip_tags($params['title']).'
                </div>
                <a href="'.strip_tags(str_replace("’", "", $params['link'])).'" target="_blank" style="display: inline-block;font-size: 16px;line-height: 26px;padding-top: 1px;color: #4A4A4A;text-decoration: underline;">
                    '.strip_tags($params['text']).'
                </a>
            </div>
        </iframe>
    </figure>';
}

function button_shortcode_ai( $array ) {
    $array['button'] = 'add_shortcode_button_iframe';
    $array['readmore'] = 'add_shortcode_readmore_iframe';
    return $array;
}

add_filter( 'wpna_facebook_article_pre_the_content_filter', 'prepare_tables_for_ia' );
add_filter( 'wpna_facebook_article_setup_wrap_shortcodes_override_tags', 'button_shortcode_ai' );

/*------------------------------------*\
    Rewrites for production
\*------------------------------------*/

if (!defined('WP_TEST')) {
    function my_custom_admin_url($path) {
        $path = str_replace('blog/wp-admin', 'wp-admin', $path);
        return str_replace('wp-admin', 'blog/wp-admin', $path);
    }

    add_filter('admin_url', 'my_custom_admin_url');
    add_action('wp_loaded', 'admin_start');

    function admin_start() {
        if (strstr($_SERVER['REQUEST_URI'],'wp-admin')) {
            ob_start("admin_replace");
        }
    }

    add_action('shutdown', 'admin_end');

    function admin_end()   {
        if (strstr($_SERVER['REQUEST_URI'],'wp-admin')) {
            ob_end_flush();
        }
    }

    function admin_replace($buffer) {
        $buffer = str_replace('/blog/wp-admin/', '/wp-admin/', $buffer);
        return str_replace('/wp-admin/', '/blog/wp-admin/', $buffer);
    }
}
?>
