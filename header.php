<?php
/**
 * The Header for aviasales theme.
 */
  global $post;
  $headerPost = getHeaderPost();
  $bg_image = is_single() || is_home() ? getImageSrcFromSinglePost($headerPost) : '';
  $bg_style_tpl = ' style="background-image: url(\'%s\');"';
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="yandex-verification" content="cd0ec683d7387bba" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if (!empty($bg_image)): ?>
<?php endif; ?>
<?php wp_head(); ?>
<meta property="og:image" content="<?php echo $bg_image; ?>" />
<meta http-equiv="x-dns-prefetch-control" content="on" />

<link rel="shortcut icon" href="//www.aviasales.ru/favicon.ico" type="image/x-icon">
<script type="text/javascript" src="//vk.com/js/api/share.js?11" charset="windows-1251" async defer></script>
<meta name="wot-verification" content="44a910f90cc7d3e7c987"/>

<?php if (!is_home()) { ?>
    <script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=mjRN0stPJtsm0NXQ"></script>
<?php } ?>

</head>
<body <?php body_class(); ?> tabindex="-1">

  <header class="header">
    <div class="header__container" <?php if (!empty($bg_image)) { printf($bg_style_tpl, $bg_image); } ?>>
      <div class="header__inner">
        <div class="header__links" <?php if (!empty($bg_image)) { printf($bg_style_tpl, $bg_image); } ?>>
          <div class="header__links-inner">
            <div class="logo">
              <a class="logo__link" href="/blog"><img class="logo__image" src="<?php echo get_template_directory_uri() ?>/img/logo.svg" alt=""> <span class="is-tablet-hidden">aviasales</span> <img class="logo__image is-tablet-hidden" src="<?php echo get_template_directory_uri() ?>/img/sublogo.svg" alt=""><span class="is-tablet-shown is-mobile-hidden logo__blog-text">blog</span></a>
            </div>
            <div class="services-links">
              <ul class="services-links__list">
                <li class="services-links__item"><a class="services-links__link services-links__plane" href="https://www.aviasales.ru/">Авиабилеты</a></li>
                <li class="services-links__item"><a class="services-links__link services-links__hotels" href="https://hotels.aviasales.ru/" data-discount="60%">Отели</a></li>
                <li class="services-links__item"><a class="services-links__link services-links__auto" target='_blank' rel='nofollow' href="http://www.rentalcars.com/Home.do?affiliateCode=aviasalescpc&preflang=ru&adplat=homepage&adcamp=cartab">Авто</a></li>
                <li class="services-links__item"><a class="services-links__link services-links__insurance" target='_blank' rel='nofollow' href="https://www.sravni.ru/vzr/?p_i_d=mantravel%7Caviasales.ru&utm_source=aviasales.ru&utm_medium=referral_link&utm_campaign=referral_link_mantravel">Страховка</a></li>
              </ul>
            </div>
            <div class="header-right">
              <div class="header-right__inner">
                <div class="header-right__subscribe is-tablet-hidden" href="#">
                  <?php if (!dynamic_sidebar('sidebar-1')): ?>
                    <!-- sidebar -->
                    <aside id="blog_subscription-2" class="widget jetpack_subscription_widget">
                      <h3 class="widget-title">Подписка на новости Aviasales</h3>
                      <form action="#" method="post" accept-charset="utf-8" id="subscribe-blog-blog_subscription-2" data-action="<?php echo get_template_directory_uri() ?>">
                        <div id="subscribe-text"><p>Введите email, чтобы подписаться на наши новости и получать уведомления о новых сообщениях мгновенно.</p></div>
                        <p id="subscribe-email">
                          <label id="jetpack-subscribe-label" for="subscribe-field-blog_subscription-2" style="clip: rect(1px 1px 1px 1px); position: absolute; height: 1px; width: 1px; overflow: hidden;">
                            E-mail адрес						</label>
                          <input type="email" name="email" required="required" class="required" value="" id="subscribe-field-blog_subscription-2" placeholder="E-mail адрес">
                        </p>

                        <p id="subscribe-submit">
                          <input type="hidden" name="action" value="subscribe">
                          <input type="hidden" name="source" value="http://blog.aviasales.ru/">
                          <input type="hidden" name="sub-type" value="widget">
                          <input type="hidden" name="redirect_fragment" value="blog_subscription-2">
                                      <input type="submit" value="Подписаться" name="jetpack_subscriptions_widget">
                        </p>
                      </form>
                    </aside>
                    <!-- sidebar -->
                  <?php endif; ?>
                  <span class="header-right__subscribe-fake-btn"></span>
                  <span class="header-right__subscribe-label">Подписаться</span>
                </div>
                <div class="navbar__menu" tabindex="-1">
                  <span class="hamburger" href="#menu">
                    <span class="hamburger__line"></span>
                    <span class="hamburger__line"></span>
                    <span class="hamburger__line"></span>
                  </span>
                  <div class="top-menu"><div class="top-menu__inner"><div class="top-menu__arrow-up"></div><div class="top-menu__container"><div class="top-menu__list --services"><a class="top-menu__item --calendar" href="//www.aviasales.ru/calendar"><div class="item-name">Календарь низких цен</div><div class="top-menu__services-icon"></div></a><a class="top-menu__item --map" href="//map.aviasales.ru"><div class="item-name">Карта низких цен</div><div class="top-menu__services-icon"></div></a><a class="top-menu__item --offers" href="//www.aviasales.ru/offers"><div class="item-name">Спецпредложения</div><div class="top-menu__services-icon"></div></a><a class="top-menu__item --direct-map" target="_blank" href="//map-direct-flights.aviasales.ru/?utm_source=internal&amp;utm_medium=display&amp;utm_campaign=map-direct-flights&amp;utm_content=link_main_page&amp;utm_nooverride=1"><div class="item-name">Карта прямых рейсов</div><div class="top-menu__services-icon"></div></a><a class="top-menu__item --top100" target="_blank" href="//top100.aviasales.ru/?utm_source=aviasales&amp;utm_campaign=as_marketing_top100&amp;utm_medium=display&amp;utm_content=mainmenu"><div class="item-name">Топ 100 билетов</div><div class="top-menu__services-icon"></div></a></div><div class="top-menu__list --help-and-info"><a class="top-menu__item" href="//www.aviasales.ru/mag">Блог</a><a class="top-menu__item" href="//www.aviasales.ru/mag/kak-polzovatsya-saitom">Помощь</a><a class="top-menu__item" href="//www.aviasales.ru/about">О компании</a><a class="top-menu__item" href="//www.aviasales.ru/vacancies">Вакансии</a></div><div class="top-menu__list --company"><a class="top-menu__item" href="//www.aviasales.ru/airlines">Авиакомпании</a><a class="top-menu__item" href="//www.aviasales.ru/countries">Страны</a><a class="top-menu__item" href="//www.aviasales.ru/cities">Города</a><a class="top-menu__item" href="//www.aviasales.ru/airports">Аэропорты</a></div><div class="top-menu__list --apps"><a class="top-menu__item app-link" href="//www.aviasales.ru/ios?c=top_menu" target="_blank"><svg width="18" height="22" viewBox="0 0 18 22" xmlns="http://www.w3.org/2000/svg"><title>Fill 1</title><path d="M15.034 11.688c-.026-2.784 2.283-4.122 2.388-4.187-1.31-1.92-3.336-2.132-4.05-2.185-1.82-.134-3.363 1.012-4.224 1.012-.875 0-2.222-.983-3.65-.956C3.62 5.4 1.89 6.46.924 8.13c-1.95 3.365-.498 8.353 1.4 11.085.928 1.335 2.036 2.838 3.49 2.784 1.4-.056 1.93-.902 3.622-.902s2.17.9 3.65.873c1.506-.026 2.46-1.36 3.382-2.703 1.065-1.55 1.504-3.05 1.53-3.128-.034-.013-2.937-1.122-2.966-4.452zM12.25 3.512c.773-.93 1.293-2.224 1.15-3.512-1.113.044-2.458.738-3.256 1.667-.715.823-1.342 2.14-1.172 3.402 1.24.095 2.506-.628 3.278-1.558z" fill="#FFF" fill-rule="evenodd"></path></svg></a><a class="top-menu__item app-link" href="//www.aviasales.ru/android?c=top_menu" target="_blank"><svg width="18" height="23" viewBox="0 0 18 23" xmlns="//www.w3.org/2000/svg"><title>Fill 1</title><path d="M17 7.306c-.552 0-1 .626-1 1.4v5.47c0 .774.448 1.4 1 1.4s1-.626 1-1.4v-5.47c0-.774-.448-1.4-1-1.4zm-16 0c-.552 0-1 .626-1 1.4v5.47c0 .774.448 1.4 1 1.4s1-.626 1-1.4v-5.47c0-.774-.448-1.4-1-1.4zM3 17c0 .603.494 1.058 1.09 1.058h.82v3.308c0 .785.46 1.634 1.226 1.634s1.228-.85 1.228-1.634v-3.308h3.272v3.308c0 .785.462 1.634 1.228 1.634.765 0 1.227-.85 1.227-1.634v-3.308h.82c.596 0 1.09-.455 1.09-1.058V7H3v10zM13.09.323c.066-.1.05-.23-.04-.29-.087-.063-.214-.032-.28.067l-1.185 1.734c-.78-.31-1.646-.483-2.56-.483-.912 0-1.78.174-2.558.484L5.283.1C5.215 0 5.09-.03 5 .032c-.088.06-.105.19-.037.29l1.14 1.67C4.29 2.842 3.16 4.112 3 6h12c-.162-1.887-1.24-3.157-3.05-4.008l1.14-1.67zm-6.666 4.3c-.377 0-.682-.308-.682-.688 0-.38.305-.69.682-.69.377 0 .682.31.682.69 0 .38-.305.69-.682.69zm5.29 0c-.378 0-.683-.308-.683-.688 0-.38.306-.69.683-.69.377 0 .682.31.682.69 0 .38-.305.69-.682.69z" fill="#FFF" fill-rule="evenodd"></path></svg></a><a class="top-menu__item app-link" href="//avs.io/landing" target="_blank"><svg width="18" height="17" viewBox="0 0 18 17" xmlns="http://www.w3.org/2000/svg"><title>Imported Layers</title><path d="M0 2.528l7.36-.91.004 6.44-7.357.037L0 2.528zm7.358 6.42l.006 6.433L0 14.466v-5.56l7.358.043zM18 0v8.095H8.182V1.62L18 0zm-.002 17l-9.816-1.61V8.906H18L17.998 17z" fill="#FFF" fill-rule="evenodd"></path></svg></a></div><div class="top-menu__list --mobile"><a class="top-menu__item" href="//m.aviasales.ru" target="_blank">Мобильная версия сайта</a></div></div></div></div>
                </div>
              </div>
            </div>
          </div>
          <div class="header__links-bg"></div>
        </div>

        <div class="header__content">
            <?php if ( is_category() || is_tag() ): ?>
              <h1 class="header__list-title">
                <span class="header__title-def">рубрика</span> &laquo;<?php echo single_term_title( '', false ); ?>&raquo;
                <span class="header__description"><?php echo category_description(); ?></span>
              </h1>
            <?php elseif ( is_single( $post ) || is_home() ): ?>
              <div class="header__time-tags">
                <div class="header__time"><?php echo get_the_time('j F Y', $headerPost->ID); ?></div>
                <?php echo get_the_tag_list('<div class="header__tags">', ', ', '</div>', $headerPost->ID); ?>
              </div>
              <h1 class="header__list-title"><?php echo is_home() ? $headerPost->post_title : the_title(''); ?></h1>
              <?php if ( is_home() ): ?>
                <a class="header__goto-post" href="<?php the_permalink($headerPost) ?>">Читать</a>
              <?php else: ?>
                <?php
                  $author_name = get_the_author_meta('display_name',  $post->post_author);
                  echo get_avatar($post->post_author, 140, $author_name, $author_name, array('class' => array('header__author-image')) );
                ?>
              <div class="header__author-name"><?php echo $author_name; ?></div>
              <?php endif; ?>
            <?php else: ?>
              <h1 class="header__list-title"><?php echo the_title(''); ?></h1>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </header><!-- #header-->
  <div class="go-to-top-button --is-hidden"></div>
<?php dynamic_sidebar("Breadcrumbs") ?>
